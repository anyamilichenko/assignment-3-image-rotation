// bmp.c
#include "bmp.h"
#include <stdlib.h>

static inline uint32_t get_padding_size(const struct image* img) {
    return (4 - ((img->width * sizeof(struct pixel)) % 4)) % 4;
}

enum read_status from_bmp(FILE* in, struct image* img) {


    // Чтение заголовка BMP
    struct bmp_header header;
    fread(&header, sizeof(struct bmp_header), 1, in);


    // Проверка сигнатуры BMP файла
    if (header.bfType != 0x4D42) {
        return READ_INVALID_SIGNATURE;
    }

    // Заполнение структуры img данными из заголовка
    img->width = header.biWidth;
    img->height = header.biHeight;


    // Выделение памяти под данные изображения
    img->data = malloc(img->width * img->height * sizeof(struct pixel));

    // Чтение данных изображения
    for (uint64_t i = 0; i < img->height; ++i) {
        fread(&img->data[i * img->width], sizeof(struct pixel), img->width, in);

        // Пропуск мусорных байтов (padding)
        fseek(in, get_padding_size(img), SEEK_CUR);
    }

    return READ_OK;
}


enum write_status to_bmp(FILE* out, struct image const* img) {

    // Запись заголовка BMP
    struct bmp_header header;
    uint32_t padding_size = get_padding_size(img);
    uint32_t row = img->width * sizeof(struct pixel) + padding_size;

    // Заполнение полей заголовка
    header.bfType = 0x4D42;
    header.bfileSize = sizeof(struct bmp_header) + row * img->height;
    header.bfReserved = 0;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = sizeof(struct bmp_header) - 14;  // Размер структуры без общих полей
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = 1;
    header.biBitCount = 24; // для 24-битного изображения
    header.biCompression = 0;
    header.biSizeImage = row * img->height;
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;

    // Проверка успешности записи заголовка
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) {
        perror("Error writing BMP header");
        return WRITE_ERROR;
    }
    // Запись данных изображения
    for (uint64_t i = 0; i < img->height; ++i) {
        // Проверка успешности записи данных
        if (fwrite(&img->data[i * img->width], sizeof(struct pixel), img->width, out) != img->width) {
            perror("Error writing BMP image data");
            return WRITE_ERROR;
        }

        for (uint32_t j = 0; j < padding_size; ++j) {
            // Проверка успешности записи мусорных байтов
            if (fputc(0, out) == EOF) {
                perror("Error writing BMP padding");
                return WRITE_ERROR;
            }
        }
    }

    return WRITE_OK;
}

