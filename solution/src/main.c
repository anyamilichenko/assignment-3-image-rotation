// main.c
#include "image.h"
#include "bmp.h"
#include "transform.h"
#include <stdlib.h>


int main(int argc, char *argv[]) {
    if (argc != 4) {
        fprintf(stderr, "Usage: %s <source-image> <transformed-image> <angle>\n", argv[0]);
        return 1;
    }

    const char *source_filename = argv[1];
    const char *transformed_filename = argv[2];



    int angle = atoi(argv[3]);

    if (angle % 90 != 0 || angle < -270 || angle > 270) {
        fprintf(stderr, "Error: Invalid angle. Must be one of 0, 90, -90, 180, -180, 270, -270.\n");
        return 1;
    }



    FILE *source_file = fopen(source_filename, "rb");
    if (!source_file) {
        perror("Error opening source image file");
        return 1;
    }

    struct image img;
    if (from_bmp(source_file, &img) != READ_OK) {
        fprintf(stderr, "Error: Failed to read the source image.\n");
        fclose(source_file);
        free_image(&img);
        return 1;
    }

    fclose(source_file);


    struct image transformed_img;

    // Поворот изображения
    if (angle == -90 || angle == 270) {
        transformed_img = rotate(img);
    } else if (angle == 180) {
        struct image transformed_img2 = rotate(img);
        transformed_img = rotate(transformed_img2);
        free_image(&transformed_img2);
    }else if(angle == 90){
        struct image transformed_img2 = rotate(img);
        struct image transformed_img3 = rotate(transformed_img2);
        free_image(&transformed_img2);
        transformed_img = rotate(transformed_img3);
        free_image(&transformed_img3);
    }else if (angle == 0 || angle == 360) {
        struct image transformed_img2 = rotate(img);
        struct image transformed_img3 = rotate(transformed_img2);
        free_image(&transformed_img2);
        struct image transformed_img4 = rotate(transformed_img3);
        free_image(&transformed_img3);
        transformed_img = rotate(transformed_img4);
        free_image(&transformed_img4);
//        transformed_img = img;  // Если угол 0 или кратен 360, изображение не изменяется
    } else {
        fprintf(stderr, "Error: Unsupported angle %d.\n", angle);
        free_image(&img);
        return 1;
    }

    FILE *transformed_file = fopen(transformed_filename, "wb");
    if (!transformed_file) {
        perror("Error opening transformed image file");
        free_image(&img);
        free_image(&transformed_img);
        return 1;
    }

    if (to_bmp(transformed_file, &transformed_img) != WRITE_OK) {
        fprintf(stderr, "Error: Failed to write the transformed image.\n");
        fclose(transformed_file);
        free_image(&img);
        free_image(&transformed_img);
        return 1;
    }

    free_image(&img);
    free_image(&transformed_img);
    fclose(transformed_file);
    fprintf(stdout, "Image has been transformed\n");

    return 0;
}
