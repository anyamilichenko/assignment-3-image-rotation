// image.c
#include "image.h"
#include <stdio.h>
#include <stdlib.h>



struct image create_image(uint64_t width, uint64_t height) {
    struct image img;

    img.width = width;
    img.height = height;
    img.data = malloc(width * height * sizeof(struct pixel));

    // Проверка успешности выделения памяти
    if (img.data == NULL) {
        fprintf(stderr, "Memory allocation for img->data failed\n");
        exit(EXIT_FAILURE);
    }
    return img;
}

uint64_t image_get_data_size(struct image const* img){
    return img->width * img->height;
}



void free_image(struct image* img) {
    if (img != NULL) {
        free(img->data);
        img->data = NULL;
        img->width = 0;
        img->height = 0;
    }
    else {
        fprintf(stderr, "Invalid argument(s):\n");
    }

}


