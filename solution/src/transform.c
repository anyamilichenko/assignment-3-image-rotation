// transform.c
#include "transform.h"
#include <stdlib.h>



//static uint64_t image_get_new_x(size_t i, uint64_t old_height){
//    return old_height - 1 - i;
//}
//
//static uint64_t image_get_new_y(size_t j){
//    return j;
//}

//struct image rotate( struct image const source ) {
//
//    struct image new_img = create_image(source.height, source.width);
//
//    new_img.data = malloc(sizeof(struct pixel) * image_get_data_size(&new_img));
//    for (size_t i = 0; i < source.height; i++) {
//        for (size_t j = 0; j < source.width; j++) {
//            uint64_t new_x = image_get_new_x(i, source.height);
//            uint64_t new_y = image_get_new_y(j);
//            new_img.data[new_y * source.height + new_x] = source.data[i * source.width + j];
//        }
//    }
//    return new_img;
//
//}




struct image rotate(struct image const source) {
    struct image image_rotated = create_image(source.width , source.height);


    image_rotated.width = source.height;
    image_rotated.height = source.width;

    for (uint64_t i = 0; i < source.height; ++i) {
        for (uint64_t j = 0; j < source.width; ++j) {
            image_rotated.data[j * image_rotated.width + (source.height - 1 - i)] = source.data[i * source.width + j];
        }
    }

    return image_rotated;
}

