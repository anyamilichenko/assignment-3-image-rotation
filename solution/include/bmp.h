
/*
 * можно добавить файлы
 * 1. отдельный файл для печати ошибок(я так делала)
 * 2. отдельный файл для открытия закрытия файла
 *
 * можно заменить все #define на const
 * можно задавать структуру с заголовком bmp_header напрямую в файле bmp.c
 * можно в методе для поворота задавать напрямую угол или же крутить отдельно всё без угла
 * можно сделать валидацию данных(я хз как именно не использовала)
 *
 * можно сделать штуку с копированием и удалением картинок но нужно продумать логику использования их
 *
 * можно сделать отдельные функции для подсчета padding
 * можно написать отдельный метод для подсчета чего-либо
 *
 * за основу можно взять работу стариковой, она наиболее сухая, затем уже к этому лепить методы и тд
 *
 * прохождение тестов
 */


#include "image.h"
#include <stdio.h>

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_PIXELS_ERROR,
    READ_PADDING_ERROR,
    READ_INVALID_DATA

};

enum write_status{
    WRITE_OK = 0,
    WRITE_ERROR
};

struct __attribute__((packed)) bmp_header  {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};


enum read_status from_bmp(FILE* in, struct image* img);
enum write_status to_bmp(FILE* out, struct image const* img);


